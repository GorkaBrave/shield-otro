//---------------------------------------------------------------
// Info code
//---------------------------------------------------------------

const english = {
  'look_camera': 'Look at the camera'
};

let language = '';

class Localize {

  static setLanguage(lang) {
    language = lang;
  }

  static cssClass() {
    return language;
  }

  static localize(word) {
    let translated = null;
    switch (language) {
      default:
      case 'en':
        translated = english[word];
        break;
    }

    if (!translated) {
      translated = english[word] || '';
    }

    return translated;
  }
}

export default Localize;