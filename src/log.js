import Logger, {eLogLevel} from "simple-javascript-logger/lib/Logger";

let logEnabled = true;
let logConsoleReEnabled = false;

export const LogLevel = {
  ERROR: 1,
  WARN: 2,
  INFO: 3,
  LOG: 4
};

export let logger = null;

export function setupLogLevel(logLevel) {
  logLevel = logLevel || eLogLevel.INFO;
  logger = new Logger("", logLevel);
}

export function log(args) {
  if (logEnabled) {
    logger.Log(args);
    if (logConsoleReEnabled) console.re.log(args);
  }
}

export function warn(args) {
  if (logEnabled) {
    logger.Warn(args);
    if (logConsoleReEnabled) console.re.log(args);
  }
}

export function error(args) {
  if (logEnabled) {
    logger.Error(args);
    if (logConsoleReEnabled) console.re.log(args);
  }
}

export function objectToString(object) {
  let text = '';
  Object.keys(object).forEach((key) => {
    const value = object[key];
    text += key.toString() + ": " + value.toString() + '; ';
  });

  return text;
}