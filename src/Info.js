import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Props
// function complete(landing: String)
class Info extends Component {

  render() {
    return (
        <section className={"info-container"}>
          <button className="back-button" onClick={this.backSelected}>
            <FontAwesomeIcon icon="chevron-left"/><span>back</span>
          </button>
          <div className={"logo-container"}>
        <img src={require('./logo.png')} />
          </div>
          <div className={"protect-info"}>
            Protecting your privacy<br/>
            is essential to us
          </div>
          <div className={"rows-container"}>
            {this.renderRow("info-no-picture", "We do no store your picture", "It will be deleted immediately after our AI processes it")}
            {this.renderRow("info-no-id", "We do not collect any information about you")}
            {this.renderRow("info-no-share", "We do not share any information with the websites you visist")}
          </div>
        </section>
    );
  }

  renderRow(iconClass, text, subtitle = "") {
    return (
        <div className={"row"}>
          <div className={"info-icon-container"}>
            <div className={"info-icon " + iconClass}/>
          </div>
          <div className={"info-text-container"}>
            <div className={"info-text-title"}>{text}</div>
            <div className={"info-text-subtitle"}>{subtitle}</div>
          </div>
        </div>
    );
  }

  backSelected = () => {
    this.props.complete();
  };

}

export default Info;
