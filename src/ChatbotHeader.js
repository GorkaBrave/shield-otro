import React, {Component} from 'react';

export default class ChatbotHeader extends Component {
    render() {
        return (
            <a href='/'>
            <div className={"logo-container"} style={{paddingTop: '5px',paddingLeft: '10px', width:'40%', textAlign: 'center'}}>
            <img src={require('./logo_check.svg')} />
    </div>
            </a>

        );
    };
}