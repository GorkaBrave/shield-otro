import React, {Component} from 'react';
import './App.css';
import Localize from "./localize";
import * as logger from "./log";

import Client from './Client'
import Landing from './Landing'
import Info from './Info'
import Selfie from './Selfie'
import IdCard from './IdCard'
import Chatbot from './Chatbot'

import {library} from '@fortawesome/fontawesome-svg-core'
import {faChevronLeft} from '@fortawesome/free-solid-svg-icons'
import * as posenet from "@tensorflow-models/posenet";

library.add(faChevronLeft);

const BUILD = 1;

class App extends Component {

  steps = {
    client: 'client',
    landing: 'landing',
    info: 'info',
    selfie: 'selfie',
    idCard: 'idCard',
    chatbot: 'chatbot'
  };

  constructor(props) {
    super(props);

    logger.setupLogLevel(logger.LogLevel.LOG);
    logger.log("-- App started Build " + BUILD + " --");

    this.state = {
      posenet: null,
      step: this.steps.client, ///TEST: Check starting point
      blurIn: true,
      language: 'en'
    };

    if (this.state.step !== this.steps.landing) {
      Localize.setLanguage(this.state.language);
    }

    this.loadPosenet();
  }

  async loadPosenet() {
    const net = await posenet.load();
    if (net) {
      this.setState({posenetLoaded: true});
      logger.log("Posenet loaded");
      this.setState({posenet: net});
///TEST: Simulate user age has been resolved
//       this.setState({userType: Selfie.UserType.teenager});
    } else {
      this.setState({posenetLoaded: false});
      logger.error("Posenet not loaded");
    }
  }

  render() {
    return (
        <div className={App.mainClass()}>
          {/*{App.renderBuild()}*/}
          {this.renderStep()}
        </div>
    );
  }

  renderStep() {
    switch (this.state.step) {
      default:
      case this.steps.client:
        return this.renderClient();
      case this.steps.landing:
        return this.renderLanding();
      case this.steps.info:
        return this.renderInfo();
      case this.steps.selfie:
        return this.renderSelfie();
      case this.steps.chatbot:
        return this.renderChatbot();
      case this.steps.idCard:
        return this.renderIdCard();
    }
  };

  static mainClass() {
    return 'App background ' + Localize.cssClass();
  }

  // static renderBuild() {
  //   let build = null;
  //   if (process.env.NODE_ENV === 'development') {
  //     build = <div style={{color: "white", fontSize: "8px", position: "fixed", zIndex: 10}}>{"B" + BUILD}</div>
  //   }
  //
  //   return (build);
  // }

  renderClient() {
    return (
        <Client blurIn={this.state.blurIn} complete={this.clientComplete}/>
    );
  }

  clientComplete = () => {
    this.goToStep(this.steps.landing);
  };

  renderLanding() {
    return (
        <Landing canContinue={this.state.posenet !== null}
                 complete={this.landingComplete}/>
    );
  }

  landingComplete = (info) => {
    const {selected} = info;
    if (selected === "continue") {
      this.goToStep(this.steps.selfie);
    } else if (selected === "info") {
      this.goToStep(this.steps.info);
    } else if (selected === "chatbot") {
      this.goToStep(this.steps.chatbot);
    }
  };

  renderInfo() {
    return (
        <Info complete={this.infoComplete}/>
    );
  }

  infoComplete = () => {
    this.goToStep(this.steps.landing);
  };

  renderChatbot() {
    return (
        <Chatbot complete={this.chatbotComplete}/>
    );
  }

  chatbotComplete = () => {
    this.goToStep(this.steps.landing);
  }

  renderSelfie() {
    return (
        <Selfie posenet={this.state.posenet}
                language={this.state.language}
                complete={this.selfieComplete}/>
    );
  }

  selfieComplete = (userType) => {
    if (userType === Selfie.UserType.adult) {
      this.setState({blurIn: false});
      this.goToStep(this.steps.client);
    } else if (userType === Selfie.UserType.teenager) {
      this.goToStep(this.steps.idCard);
    }
  };

  renderIdCard() {
    return (
        <IdCard complete={this.idCardComplete}/>
    );
  }

  idCardComplete = (userType) => {
    if (userType === Selfie.UserType.adult) {
      this.setState({blurIn: false});
      this.goToStep(this.steps.client);
    }
  };

  // Step control
  goToStep(name) {
    this.setState({step: name});
  }
}

export default App;
