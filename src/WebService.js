import {post, get} from 'axios';

export default class WebService {
  callback = null;

  detectFace(url, imageBase64){

    const options = {
      method: 'POST',
      body: JSON.stringify({image_data: imageBase64}),
      headers: {
        "content-type": "application/json",
      }
    };

    return fetch(url, options);
  }

  get(url, callback) {
    this.url = url;
    this.callback = callback;

    get(url).then(response => {
      this.callback({data: response.data});
    }).catch((error) => {
      this.callback({error: error});
    });
  }

  postInfo(url, headers, info, callback) {
    this.url = url;
    this.headers = headers;
    this.callback = callback;
    const formData = new FormData();
    Object.keys(info).forEach(key => formData.append(key, info[key]));
    this.uploadDataAxios(formData);
  };

  postInfoWithBlob(url, headers, info, blob, blobFilename, callback) {
    this.url = url;
    this.headers = headers;
    this.callback = callback;
    this.uploadBlob(blob, blobFilename, info);
  };

  // Private methods
  uploadBlob = (blob, blobFilename, info) => {
    // Do something with the blob object,
    // e.g. creating a multipart form for file uploads:
    let formData = new FormData();
    formData.append(blobFilename, blob, 'image.png');
    Object.keys(info).forEach(key => formData.append(key, info[key]));
    this.uploadDataAxios(formData);
  };

  uploadDataAxios = (formData) => {
    return post(this.url, formData, this.headers).then(response => {
      this.callback({data: response.data});
    }).catch((error) => {
      this.callback({error: error});
    });
  };
}
