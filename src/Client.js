import React, {Component} from 'react';

// Props
// function complete()
class Client extends Component {

  componentDidMount() {
    this.setupAnimationCallback();
  }

  render() {
    const blurClass = this.props.blurIn ? 'blurIn' : 'blurOut';

    return (
        <section id={"client-container"} className={"client-container " + blurClass}>
          <div className={"white-screen " + blurClass}/>
          <div className={"client-logo"}/>
          <div className={"lorem-text"}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Igitur neque stultorum quisquam beatus neque
            sapientium non beatus. Quae quidem sapientes sequuntur duce natura tamquam videntes; Deprehensus omnem
            poenam contemnet. Ita relinquet duas, de quibus etiam atque etiam consideret. Qui ita affectus, beatum esse
            numquam probabis; Duo Reges: constructio interrete.
          </div>
        </section>
    );
  }

  setupAnimationCallback() {
    if (this.props.blurIn) {
      let container = document.getElementById("client-container");
      container.addEventListener("animationend", () => {
        this.props.complete();
      });
    }
  }
}

export default Client;
