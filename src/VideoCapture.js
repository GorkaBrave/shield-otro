import React, {Component} from 'react';
import {bool, number, func} from 'prop-types';
import * as logger from "./log";

class VideoCapture extends Component {
  static propTypes = {
    maxSize: number.isRequired,
    face: bool,
    capturingEnabled: bool,
    cameraFrameCallback: func,
    cameraSupportedCallback: func
  };

  static defaultProps = {
    face: false,
    capturingEnabled: false
  };

  constructor(props) {
    super(props);
    this.capturingInterval = null;

    this.state = {
      hasUserMedia: false,
      capturingEnabled: false
    };
  }

  componentDidMount() {
    this.requestUserMedia();
  }

  componentWillUnmount() {
    this.stopCapturingFrames();

    if (this.state.stream) {
      this.state.stream.getVideoTracks().forEach(track => track.stop());
    }
  }

  requestUserMedia() {
    if (!navigator.mediaDevices) {
      this.setState({stream: null});
      this.props.cameraSupportedCallback(false);
      return;
    }

    navigator.mediaDevices.getUserMedia({
      audio: false,
      video: {
        width: 1920,
        height: 1080,
        facingMode: {ideal: this.props.face ? 'user' : 'environment'}
      },
    })
        .then(stream => this.handleUserMedia(stream))
        .catch(() => {
          this.setState({stream: null});
          this.props.cameraSupportedCallback(false);
        });
  }

  handleUserMedia(stream) {
    if (this.video) {
      this.video.srcObject = stream;
      this.props.cameraSupportedCallback(true);
    }

    this.setState({
      hasUserMedia: true,
      stream: stream
    });
  }

  getScreenshot() {
    if (!this.state.hasUserMedia) return null;

    return this.getCanvas();
  }

  getCanvas() {
    if (!this.state.hasUserMedia || !this.video) return null;

    if (!this.imgCtx) {
      const canvas = document.createElement('canvas');
      canvas.width = this.video.clientWidth;
      canvas.height = this.video.clientHeight;

      this.imgCanvas = canvas;
      this.imgCtx = canvas.getContext('2d');
    }

    const {imgCtx, imgCanvas} = this;
    const sourceRatio = this.video.videoWidth / this.video.videoHeight;
    const destRatio = imgCanvas.width / imgCanvas.height;
    const sw = sourceRatio < destRatio
        ? this.video.videoWidth
        : this.video.videoHeight * destRatio;
    const sh = sourceRatio < destRatio
        ? this.video.videoWidth / destRatio
        : this.video.videoHeight;
    const sx = (this.video.videoWidth - sw) / 2;
    const sy = (this.video.videoHeight - sh) / 2;
    imgCtx.drawImage(this.video, sx, sy, sw, sh, 0, 0, imgCanvas.width, imgCanvas.height);

    if (!this.resizeCanvas) {
      this.resizeCanvas = document.createElement('canvas');
    }

    const {resizeCanvas} = this;
    const ratio = imgCanvas.width / imgCanvas.height;

    if (ratio < 1) {
      resizeCanvas.height = Math.min(imgCanvas.height, this.props.maxSize);
      resizeCanvas.width = resizeCanvas.height * ratio;
    } else {
      resizeCanvas.width = Math.min(imgCanvas.width, this.props.maxSize);
      resizeCanvas.height = resizeCanvas.width / ratio;
    }

    const resizeCtx = resizeCanvas.getContext('2d');

    resizeCtx.drawImage(imgCanvas, 0, 0, resizeCanvas.width, resizeCanvas.height);
    return resizeCanvas;
  }

  render() {
    if (this.state.stream === null) {
      return null;
    }

    this.capturing(this.props.capturingEnabled);

    return (
        <video className={this.props.className}
               style={{
                 objectFit: 'cover',
               }}
               autoPlay
               playsInline
               ref={(ref) => {
                 this.video = ref;
               }}
        />
    );
  }

  capturing(enabled) {
    if (enabled) {
      this.startCapturingFrames();
    } else {
      this.stopCapturingFrames();
    }
  }

  startCapturingFrames() {
    if (this.capturingInterval === null) {
      logger.log("capturing started");
      const frameRate = 10;
      this.capturingInterval = setInterval(() => {
        const canvas = this.getScreenshot();
        this.props.cameraFrameCallback(canvas);
      }, (1000 / frameRate));
    }
  }

  stopCapturingFrames() {
    if (this.capturingInterval !== null) {
      logger.log("capturing stopped");
      clearInterval(this.capturingInterval);
      this.capturingInterval = null;
    }
  }
}

export default VideoCapture;
